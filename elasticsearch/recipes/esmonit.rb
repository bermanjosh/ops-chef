service "monit" do
  supports restart: true, reload: true
  action [:enable, :start]
end

template "/etc/monit/conf.d/elasticsearch.monitrc" do
  source "elasticsearch.monitrc.erb"
  mode 0440
  owner "root"
  group "root"
  notifies :reload, "service[monit]"
end
