name             "elasticsearch"

maintainer       "karmi"
maintainer_email "karmi@karmi.cz"
license          "Apache"
description      "Installs and configures elasticsearch"
long_description "Installs and configures elasticsearch"
version          "0.3.13"

provides 'elasticsearch'
provides 'elasticsearch::plugins'